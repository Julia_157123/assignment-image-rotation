#ifndef __ROTATE_H
#define __ROTATE_H

#include "image.h"

void rotate(struct image * source);

#endif //__ROTATE_H
