#ifndef UTIL_H
#define UTIL_H

enum read_status read_bmp_image(struct image* img, const char* file_name);

enum write_status write_bmp_image(struct image* img, const char* file_name);

#endif
