#ifndef __IMAGE_H
#define __IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void image_create(struct image* img, uint64_t height, uint64_t width);

void image_destruct(struct image* img);

#endif
