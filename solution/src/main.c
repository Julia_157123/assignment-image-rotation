#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include "../include/util.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    struct image im;
    im.data = NULL;

    if (read_bmp_image(&im, argv[1])) {
        return 1;
    }

    rotate(&im);

    if (write_bmp_image(&im, argv[2])) {
        return 1;
    }
    image_destruct(&im);
    return 0;
}
