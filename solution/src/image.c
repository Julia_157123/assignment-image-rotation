#include "../include/image.h"
#include <malloc.h>
#include <stdio.h>

void image_create(struct image* img, uint64_t height, uint64_t width) {
    img->width = width;
    img->height = height;
    img->data = (struct pixel*)malloc(width * height * sizeof(struct pixel));
}

void image_destruct(struct image* img) {
    if (img->data != NULL) {
        free(img->data);
    }
}
