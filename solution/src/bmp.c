#include "../include/image.h"
#include "../include/bmp.h"
#include <stdbool.h>

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static struct bmp_header bmp_header_create(uint64_t height, uint64_t weight) {
    return (struct bmp_header){19778, height * weight * 3 + 70, 0, 54, 40, weight, height, 1, 24, 0, 180, 2834, 2834, 0, 0};
}

bool check_bmp_header(struct bmp_header* header) {
    return header->bfType != 19778;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    size_t read_symbols;

    read_symbols = fread(&header, sizeof(header), 1, in);
    if (ferror(in) || read_symbols < 1) {
        return READ_INVALID_BITS;
    }

    if (check_bmp_header(&header)) {
        return READ_INVALID_SIGNATURE;
    }

    struct image new_image;

    image_create(&new_image, header.biHeight, header.biWidth);
    char buff[4];
    for (size_t i = 0; i < (size_t)new_image.height; i++) {
        read_symbols = fread(new_image.data + i * new_image.width, sizeof (struct pixel), new_image.width, in);
        if (ferror(in) || read_symbols < new_image.width) {
            image_destruct(&new_image);
            return READ_INVALID_BITS;
        }
        read_symbols = fread(buff, 1, new_image.width % 4, in);
        if (ferror(in) || read_symbols < new_image.width % 4) {
            image_destruct(&new_image);
            return READ_INVALID_BITS;
        }
    }

    if (ferror(in)) {
        image_destruct(&new_image);
        return READ_INVALID_BITS;
    }
    image_destruct(img);
    img->width = new_image.width;
    img->height = new_image.height;
    img->data = new_image.data;
    return READ_OK;
}

size_t padding(size_t width) {
    return width % 4;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = bmp_header_create(img->height, img->width);
    fwrite(&header, sizeof (header), 1, out);
    if (ferror(out)) {
        return WRITE_ERROR;
    }
    for (size_t i = 0; i < (size_t)img->height; i++) {
        fwrite(img->data + i * img->width, sizeof (struct pixel), img->width, out);
        if (ferror(out)) {
            return WRITE_ERROR;
        }
        fwrite("\0\0\0\0", 1, padding(img->width), out);
        if (ferror(out)) {
            return WRITE_ERROR;
        }
    }
    if (ferror(out)) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}
