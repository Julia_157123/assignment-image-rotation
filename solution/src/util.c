#include "../include/image.h"
#include "../include/bmp.h"

static const char* read_error_messages[] = {"READ_OK_message", "Read Error.(READ_INVALID_BITS)", "Read Error.(READ_INVALID_SIGNATURE)", "Ride Error.(Error while read file)"};
static const char* write_error_messages[] = {"WRITE_OK_message", "Write Error."};

enum read_status read_bmp_image(struct image* img, const char* file_name) {
    FILE* in;

    in = fopen(file_name, "rb");

    if (in == NULL) {
        fprintf(stderr, "Read file error.\n");
        return 1;
    }

    enum read_status status = from_bmp(in, img);
    
    if (status) {
        printf("%s", read_error_messages[status]);
        return status;
    }
    fclose(in);

    return status;
}

enum write_status write_bmp_image(struct image* img, const char* file_name) {
    FILE* out;
    out = fopen(file_name, "wb");
    if (out == NULL) {
        fprintf(stderr, "Write file error.\n");
        return WRITE_ERROR;
    }
    enum write_status status = to_bmp(out, img);
    if (status) {
    	printf("%s", write_error_messages[status]);
        return status;
    }
    fclose(out);
    return WRITE_OK;
}

