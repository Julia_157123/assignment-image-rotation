#include "../include/image.h"
#include "../include/rotate.h"
#include <malloc.h>

static size_t address(const struct image* img, size_t i, size_t j) {
    return i * img->width + j;
}

void rotate(struct image * source) {
    struct image temp;
    image_create(&temp, source->width, source->height);
    size_t id = 0;
    for (size_t j = 0; j < source->width; j++) {
        for (size_t i = 0; i < source->height; i++) {
            temp.data[id++] = source->data[address(source, source->height - i - 1, j)];
        }
    }
    image_destruct(source);
    source->data = temp.data;
    source->height = temp.height;
    source->width = temp.width;
}
